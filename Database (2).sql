create database ramyasree;

use ramyasree;

create table book(id int(2) primary key,title varchar(20),author varchar(20),year int(4));
insert into book values(1,'A commentry','Apoorva',1981);
insert into book values(2,'cheque boo','vasdev mohi',1980);
insert into book values(3,'celestial bodies','jokha alharthi',1990);
insert into book values(4,'the overstory','Richard',1970);

select * from book;


create table user(id int(2) primary key,email varchar(30),name varchar(30),password varchar(20));

insert into user values(1,'ramya@gmail.com','ramya','ramya');

insert into user values(2,'sree@gmail.com','sree','sree');

insert into user values(3,'raju@gmail.com','raju','raju');

select * from user;

create table loginuser(email varchar(30),password varchar(20));

insert into loginuser values(1,'ramya@gmail.com','ramya','ramya');

insert into loginuser values(2,'sree@gmail.com','sree','sree');

select * from loginuser;
